/* 
Name - string (required)
Email - string (required)
password - string (required, minimum 6 characters)
isAdmin - boolean (default false)
isActive - boolean (default true)
*/
const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'First name and last name is required'],
  },
  email: {
    type: String,
    required: [true, 'Email cannot be empty'],
  },
  password: {
    type: String,
    min: [6, 'Minimum 6 characters'],
    required: [true, 'Password cannot be empty'],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('User', userSchema);
