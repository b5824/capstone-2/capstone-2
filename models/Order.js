/* 
totalAmount - number
purchasedOn - Date (default to current date of creation)
user: [
    {
        userId : string (required)
        orders: [ 
            {
            productId: [string (required)]
            totalAmount: Number
            }
        ]
    }
]

*/
const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  user: [
    {
      userId: String,
    },
  ],
  product: [
    {
      productId: String,
      totalAmount: Number,
    },
  ],
});

module.exports = mongoose.model('Order', orderSchema);
