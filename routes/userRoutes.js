const express = require('express');
const router = express.Router();

//! Modules
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');
const { verify } = auth;

router.post('/registerUser', userControllers.registerUser);
router.get('/login', verify, userControllers.loginUser);

module.exports = router;
