const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('./models/User');
const secret = 'AMAZONAPI';

// const verify = (req, res, next) => {
//   const { password } = req.body;

//   //   bcrypt.compareSync(password, );
//     User.find({})
// };

let verify = (req, res, next) => {
  let token = req.headers.authorization;

  if (typeof token === 'undefined') {
    return res.send({ auth: 'Failed. No token' });
  } else {
    token = token.split(' ')[1];

    jwt.verify(token, secret, (err, decodedToken) => {
      if (err) {
        return res.send({
          auth: 'Failed',
          message: err.message,
        });
      } else {
        req.user = decodedToken;

        next();
      }
    });
  }
};

module.exports = {
  verify,
};
