//! Dependencies
const bcrypt = require('bcryptjs');

//! Modules
const User = require('../models/User');

let registerUser = (req, res) => {
  //* Destructuring req.body
  const { name, email, password } = req.body;

  User.find({ email })
    .then((result) => {
      if (result.length === 0) {
        const hashPassword = bcrypt.hashSync(password, 10);
        const newUser = new User({ name, email, password: hashPassword });

        newUser
          .save()
          .then((result) => res.send(result))
          .catch((err) => res.send(err));
      } else {
        res.send('Email already exists!');
      }
    })
    .catch((err) => res.send(err));
};

const loginUser = (req, res) => {
  const { email } = req.body;

  User.find({ email })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports = {
  registerUser,
  loginUser,
};
